// stolen from https://stackoverflow.com/a/11381730
function detectMobile() {
	const toMatch = [
		/Android/i,
		/webOS/i,
		/iPhone/i,
		/iPad/i,
		/iPod/i,
		/BlackBerry/i,
		/Windows Phone/i,
	];

	return toMatch.some(toMatchItem => navigator.userAgent.match(toMatchItem));
}

let mobileShift = false;
function simulateKeypress(key) {
	document.dispatchEvent(new KeyboardEvent("keydown", { key, shiftKey: mobileShift }));
}

const isMobile = detectMobile();
