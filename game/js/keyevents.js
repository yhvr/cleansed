let lastDir = "";
let keepMoving = false;

function actuallyMove(key) {
	const prev = [...game.coords];

	switch (key) {
		case "arrowup":
		case "w":
			game.coords[1]--;
			break;
		case "arrowright":
		case "d":
			game.coords[0]++;
			break;
		case "arrowdown":
		case "s":
			game.coords[1]++;
			break;
		case "arrowleft":
		case "a":
			game.coords[0]--;
			break;
		default:
			return;
	}

	redraw = true;

	if (
		lastCollision[0] !== game.coords[0] ||
		lastCollision[1] !== game.coords[1]
	)
		lastCollision = [null, null];
	if (!canMoveTo(...game.coords)) game.coords = prev;

	lastDir = key;

	checkShardCollection();
}

document.addEventListener("keydown", e => {
	switch (e.key) {
		case "o":
			app.optionsOpen = true;
			break;
		case "e":
			if (game.upgs[32]) keepMoving = !keepMoving;
			break;
		case "Escape":
			app.optionsOpen = false;
			app.shopOpen = false;
			app.autoOpen = false;
			app.luckOpen = false;
			break;
		case "mobile_shift":
			mobileShift = !mobileShift;
			app.mobileShift = mobileShift;
			break;
	}

	let times = e.shiftKey && game.upgs[12] ? 2 : 1;
	for (let i = 0; i < times; i++) actuallyMove(e.key.toLowerCase());

	app.$forceUpdate();
});

window.setInterval(() => {
	if (!game.upgs[32]) return;
	if (!keepMoving) return;
	actuallyMove(lastDir);
}, 50);
