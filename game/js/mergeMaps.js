function mergeMaps(locked, unlocked) {
	const out = locked.map((row, y) => {
		return row.map((tile, x) => {
			return tile === " " ? unlocked[y][x] : tile
			// * if (tile !== " ") out[y][x] = tile;
		});
	});

	return out;
}
