let isEnd = false;

const allBlockData = {
	"♦": {
		name: "Home Crystal",
		desc: "Shining light on everything. Also lets you access the options.",
	},
	"⌂": {
		name: "Shop Crystal",
		desc: "Shining light on everything. Also lets you access the shop.",
	},
	"⚙": {
		name: "Automation Crystal",
		desc: "Shining light on everything. Also lets you access automation.",
	},
	"★": {
		name: "Luck Crystal",
		desc:
			"Shining light on everything. Also lets you access the luck-increaser-thingy.",
	},
	1: {
		name: "Very Weak Darkness",
		desc:
			"Not very strong. You could modify your crystal to get rid of it with just <strong>1 shard</strong>.",
	},
	2: {
		name: "Weak Darkness",
		desc:
			"A little stronger, but still not too much. You could modify your crystal to get rid of it with <strong>10 shards</strong>.",
	},
	3: {
		name: "Mild Darkness",
		desc:
			"A bit strong. You could get rid of it with <strong>100 shards</strong>.",
	},
	4: {
		name: "Darkness",
		desc:
			"Regular darkness. You could get rid of it with <strong>10,000 shards</strong>.",
	},
	5: {
		name: "Somewhat Strong Darkness",
		desc: "You could get rid of it with <strong>100,000 shards</strong>.",
	},
	6: {
		name: "Strong Darkness",
		desc: "You could get rid of it with <strong>1,000,000 shards</strong>.",
	},
	7: {
		name: "Very Strong Darkness",
		desc:
			"You could get rid of it with <strong>10,000,000 shards</strong>.",
	},
	8: {
		name: "Extremely Strong Darkness",
		desc:
			"You could get rid of it with <strong>100,000,000 shards</strong>.",
	},
	9: {
		name: "Insane Darkness",
		desc:
			"How is this so strong??? You could get rid of it with <strong>10,000,000,000 shards</strong>.",
	},
	0: {
		name: "Darkness Core",
		desc:
			"This is it. The cause of all this. You could get rid of it with <strong>100,000,000,000 shards</strong>.",
	},
	"#": {
		name: "Barrier Block",
		desc: "The edge of the map.",
	},
	"⋂": {
		name: "Door",
		desc: "A door. Moves you between maps."
	}
};

const blockData = {
	name: "Not sure what to do?",
	desc:
		'Wait a little bit until you see your first glass shard. Than try moving (using the arrow keys) to a tile that says "1" twice in a row to cleanse it.',
};

const unlockableTiles = `123456789`.split("");

function blockAction(coords, tile) {
	if (unlockableTiles.includes(tile)) unlockTile(coords, tile);
	if (tile === "⋂") {
		door(coords);
		return true;
	}
	if (tile === "♦" && !keepMoving) openUI("options");
	if (tile === "⌂" && !keepMoving) openUI("shop");
	if (tile === "⚙" && !keepMoving) openUI("auto");
	if (tile === "★" && !keepMoving) openUI("luck");
	if (tile === "0" && !keepMoving) tryEndgame();
	if (tile === "!" && !keepMoving)
		window.open("https://yhvr.me/ego", "_blank");
	if (tile === "?" && !keepMoving) window.open("https://yhvr.me/", "_blank");
	if (tile === "✔" && !keepMoving) buffReset();
	if (tile === "✘" && !keepMoving) hardReset();
	return false;
}

const tileCosts = [
	100_000_000_000,
	1,
	10,
	100,
	10_000,
	100_000,
	1_000_000,
	10_000_000,
	100_000_000,
	10_000_000_000,
];

function unlockTile([y, x], tile) {
	const cost = tileCosts[tile];
	if (game.shards < cost) return;
	game.shards -= cost;
	game.maps[game.currentMap][x][y] = uncoveredMaps[game.currentMap][x][y];

	game.coords = [y, x];
}

function tryEndgame() {
	if (game.shards < tileCosts[0]) return;
	if (game.maps[game.currentMap].some(n => n.join("").match(/[1-9]/gu))) {
		keepMoving = false;
		return alert("Clear everything else first!");
	}

	isEnd = true;
	app.isEnd = true;
	updateCanvasRoom();

	setTimeout(() => {
		game.coords = [10, 6];
	}, 0);
}
