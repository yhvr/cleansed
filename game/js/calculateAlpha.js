let fadeDistance = 6;
let crystalFadeDistance = 10;
let sweepFadeDistance = 3;
const crystalCoords = {};
const crystalTiles = ["♦", "⌂", "⚙", "★"];

// Find crystals
for (const key in uncoveredMaps) {
	crystalCoords[key] = [];
	uncoveredMaps[key].forEach((row, y) => {
		row.forEach((tile, x) => {
			if (crystalTiles.includes(tile)) crystalCoords[key].push([x, y]);
		});
	});
}

function calculateAlpha([x2, y2]) {
	const [x, y] = game.coords;
	let alpha =
		Math.max(0, fadeDistance - ((x - x2) ** 2 + (y - y2) ** 2) ** 0.5) *
		(255 / fadeDistance);

	// Crystal Light
	crystalCoords[game.currentMap].forEach(([cx, cy]) => {
		alpha +=
			Math.max(
				0,
				crystalFadeDistance - ((cx - x2) ** 2 + (cy - y2) ** 2) ** 0.5
			) *
			(255 / crystalFadeDistance);
	});

	// Auto Light
	if (sweepRow !== -1) {
		alpha +=
			Math.max(0, sweepFadeDistance - Math.abs(sweepRow - y2)) *
			(255 / sweepFadeDistance);
	}

	alpha = Math.min(alpha, 255).toString(16);

	if (alpha.length > 2) alpha = alpha.substring(0, 2);
	if (alpha.endsWith(".")) alpha = alpha.substring(0, 1);
	if (alpha.length < 2) alpha = "00";

	return alpha;
}
