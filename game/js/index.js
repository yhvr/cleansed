let game = {
	shards: 1000,
	get brightness() {
		let out = Math.max(5, Math.log10(game.shards) ** 0.5 * 10);
		return isEnd ? 100000 : out === Infinity ? 10 : out + 5;
	},
	set brightness(x) {
		// void
	},
	maps: JSON.parse(JSON.stringify(mergedMaps)),
	upgs: {},
	autoSpeed: 0,
	autoCost: 300,
	luckTokens: 0,
	luckCost: 100_000,
	currentMap: "home",
	coords: [1, 1]
};

let replayMult = 1;

function openUI(ui) {
	const UIs = ["shop", "options", "auto", "luck"];
	UIs.forEach(UI => (app[`${UI}Open`] = false));
	app[`${ui}Open`] = true;
}

const app = new Vue({
	el: "#app",
	data: {
		lastCollision,
		data: blockData,
		game,
		shopOpen: false,
		optionsOpen: false,
		autoOpen: false,
		luckOpen: false,
		UPGRADES,
		buyUpgrade,
		buyAuto,
		buySuperAuto,
		getShardChance,
		getShardGain,
		luckify,
		isEnd,
		isMobile,
		simulateKeypress,
		mobileShift,
	},
	computed: {
		brightness() {
			return this.game.brightness;
		},
		hardReset() {
			try {
				return hardReset;
			} catch (_) {
				return () => {};
			}
		},
	},
	watch: {
		brightness(n) {
			fadeDistance = 6 * (n / 20);
			crystalFadeDistance = 10 * (n / 20);
			sweep = 3 * (n / 20);
		},
	},
});

// SHARD PRODUCTION
let lastUpdate = Date.now();

const productionTiles = [" ", "."]
function update([width, height]) {
	const now = Date.now();
	if (now < lastUpdate + 1000) return;
	lastUpdate = now;
	for (let y = 0; y < height; y++) {
		for (let x = 0; x < width; x++) {
			const tile = game.maps[game.currentMap][y][x];
			if (productionTiles.includes(tile)) {
				const rng = Math.random();
				if (rng > getShardChance(coveredMaps[game.currentMap][y][x])) {
					game.maps[game.currentMap][y][x] = "_";
					if (game.autoSpeed === -1) collectShard(x, y);
					else redraw = true;
				}
			}
		}
	}
}

function getShardChance(tile) {
	let out = 1 / 100;

	if (game.upgs[21] && tile > 1) out *= 2;
	if (game.upgs[22] && tile === "3") out /= 10;
	if (game.luckTokens >= 1) out *= game.luckTokens + 1;

	return 1 - out;
}

//SHARD COLLECTION
function checkShardCollection() {
	const [x, y] = game.coords;
	if (game.maps[game.currentMap][y][x] === "_") {
		game.shards += getShardGain(coveredMaps[game.currentMap][y][x]);
		game.maps[game.currentMap][y][x] = uncoveredMaps[game.currentMap][y][x];
	}
}

function collectShard(x, y) {
	if (game.maps[game.currentMap][y][x] === "_") {
		game.shards += getShardGain(coveredMaps[game.currentMap][y][x]);
		game.maps[game.currentMap][y][x] = uncoveredMaps[game.currentMap][y][x];
	}
}

function getShardGain(tile) {
	let out = replayMult;

	if (game.upgs[11]) out *= 2;
	if (game.upgs[13] && tile > 1) out *= 3;
	if (game.upgs[22] && tile === "3") out *= 100;
	if (game.upgs[23] && tile ** 2 > 1) out *= tile ** 2;
	if (game.upgs[31] && tile === "4") out *= 10;
	if (game.upgs[33] && tile >= 6) out *= Math.round(tile ** (tile / 2));
	if (game.luckTokens >= 1)
		out *= Math.round(Math.log(game.luckTokens ** game.luckTokens) + 1);

	return out;
}
