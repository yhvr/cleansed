const UPGRADES = {
	rows: 3,
	cols: 3,
	11: {
		title: "Reflection",
		desc: "Double shard gain from shards on the ground.",
		cost: 10,
		shown: () => true,
	},
	12: {
		title: "Better Shoes",
		desc: "Gain the ability to move 2 blocks at a time by holding [Shift].",
		cost: 10,
		shown: () => game.upgs[11],
	},
	13: {
		title: "Make it worth it",
		desc: "Multiply shard gain by 3 if it came from a [2] or higher.",
		cost: 100,
		shown: () => game.upgs[11],
	},
	21: {
		title: "It's just luck, I'm telling you man",
		desc:
			"Double the chance of glass shards appearing on tiles that are [2] or higher.",
		cost: 250,
		shown: () => game.upgs[13],
	},
	22: {
		title: "A risky move",
		desc:
			"Divide the chance of glass shards appearing by 10 on [3], but multiply their gain by 100.",
		cost: 2_500,
		shown: () => game.upgs[13],
	},
	23: {
		title: "The bigger, the better",
		desc: "Multiply shard gain based on the tile it came from. (n^2)",
		cost: 25_000,
		shown: () => game.upgs[13],
	},
	31: {
		title: "[4] still isn't powerful enough",
		desc: "Multiply shard gain by 10 if it came from a [4].",
		cost: 500_000,
		shown: () => game.upgs[23],
	},
	32: {
		title: "i'm lazy",
		desc: "Press [E] to move in the direction you last moved 20 times/s.",
		cost: 10_000_000,
		shown: () => game.upgs[31],
	},
	33: {
		title: "The biggerer, the betterer",
		desc: "Multiply shard gain based on the tile it came from if the tile is [6] or higher. (n^(n/2))",
		cost: 25_000_000,
		shown: () => game.upgs[32],
	},
};

function buyUpgrade(id) {
	const { cost } = UPGRADES[id];
	if (cost > game.shards) return;
	if (game.upgs[id]) return;
	game.shards -= cost;
	game.upgs[id] = true;
}
