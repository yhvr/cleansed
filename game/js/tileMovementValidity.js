const illegalTiles = "1234567890♦⌂⚙★#⋂".split("");
// const endIllegalTiles = ["!", "?", "✔", "✘"];
let lastCollision = [null, null];

function canMoveTo(x, y) {
	const map = game.maps[game.currentMap];

	let isCollision = false;
	if (map[y] === undefined || map[y][x] === undefined) isCollision = true;
	if (x < 0 || y < 0) isCollision = true;
	// if (x > map.length - 1 || y > map[0].length - 1) return false;
	let tile = map[y]?.[x];
	if (illegalTiles.includes(tile))
		isCollision = true;

	if (lastCollision[0] === x && lastCollision[1] === y && isCollision) {
		return blockAction([x, y], tile);
	}

	if (isCollision) {
		lastCollision = [x, y];
		app.lastCollision = [x, y];

		tile = tile ?? "#";

		if (allBlockData[tile]) {
			const data = allBlockData[tile];
			blockData.name = `<span style="color: ${
				fillOverrides[game.currentMap]?.[tile] ?? fillColors[tile] ?? "#FFFFFF"
			}">${tile}</span> | ${data.name}`;
			blockData.desc = data.desc;
		} else {
			blockData.name = tile;
			blockData.desc = "You shouldn't be seeing this.";
		}

		blockData.tile = tile;
	}

	return !isCollision;
}
