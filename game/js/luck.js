function luckify() {
	if (game.luckCost > game.shards) return;
	game.shards -= game.luckCost;
	game.luckCost *= 10;
	game.luckCost = Math.round(game.luckCost);
	game.luckTokens++;
}