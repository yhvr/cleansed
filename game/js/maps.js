// no pedos here

const uncoveredMaps = {
	home: `########
#......#
#......⋂
#......#
########`,
	/**
	 * IN ORDER, l->r, t->b:
	 * green god place
	 * red god place
	 * light shop, automation, "potions" (temp buffs), blue god place
	 * normal shop, luck shop
	 */
	street: `##################
######^.....######
###########.^#####
####^###^#^.##^###
&................#
##^####.####^#####
#######.##########
#######⋂##########
##################`,
};

const coveredMaps = {
	home: `        
   1111 
  11111 
 111111 
        `,
	street: `                  
       55555      
           5      
           4      
       1223345555 
                  
                  
                  
                  `,
};

const mergedMaps = {};

function parseMap(map) {
	// [...x] = x.split("") but unicode
	return map.split("\n")
		.map(n => [...n]);
}

for (const key in uncoveredMaps) {
	coveredMaps[key] = parseMap(coveredMaps[key])
	uncoveredMaps[key] = parseMap(uncoveredMaps[key])

	mergedMaps[key] = mergeMaps(
		coveredMaps[key],
		uncoveredMaps[key]
	);
}