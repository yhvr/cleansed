let sweepRow = -1;
let lastSweepChange = Date.now();

function updateAuto([width, height]) {
	const now = Date.now();
	if (game.autoSpeed > 0 && now > lastSweepChange + game.autoSpeed) {
		lastSweepChange = now;
		sweepRow++;
		if (sweepRow >= height) sweepRow = 0;
		for (let i = 0; i < width; i++) {
			collectShard(i, sweepRow);
		}
	}
}

function buyAuto() {
	if (game.autoCost > game.shards) return;
	if (game.autoSpeed === 0) game.autoSpeed = 1000;
	else game.autoSpeed /= 1.5;
	game.shards -= game.autoCost;
	game.autoCost *= 2;
}

function buySuperAuto() {
	if (game.shards < 2_500_000_000) return;
	game.shards -= 2_500_000_000;
	game.autoSpeed = -1;

	sweepRow = Infinity;

	game.maps[game.currentMap] = game.maps[game.currentMap].map((row, y) =>
		row.map((tile, x) =>
			tile === "_" ? uncoveredMaps[game.currentMap][y][x] : tile
		)
	);
}
