let shouldSave = true;
window.setInterval(() => {
	if (shouldSave)
		window.localStorage.setItem("cleansed_save", JSON.stringify(game));
}, 5000);

if (window.localStorage.getItem("cleansed_save")) {
	const save = JSON.parse(window.localStorage.getItem("cleansed_save"));
	for (const prop in save) {
		if (save[prop] instanceof Object)
			for (const subprop in save[prop])
				game[prop][subprop] = save[prop][subprop];
		else game[prop] = save[prop];
	}

	fadeDistance = 6 * (game.brightness / 20);
	crystalFadeDistance = 10 * (game.brightness / 20);
	sweepFadeDistance = 3 * (game.brightness / 20);

	sweepRow = game.autoSpeed === -1 ? Infinity : -1;
}

if (window.localStorage.getItem("cleansed_speedup")) {
	replayMult = Number(window.localStorage.getItem("cleansed_speedup"));
}

function buffReset() {
	if (!confirm("Are you sure you want to reset?")) return;
	window.localStorage.removeItem("cleansed_save");
	window.localStorage.setItem(
		"cleansed_speedup",
		(window.localStorage.getItem("cleansed_speedup") ?? 1) * 2
	);
	window.location.reload();
}

function hardReset() {
	if (!confirm("Are you sure you want to hard reset?")) return;
	shouldSave = false;
	window.localStorage.removeItem("cleansed_save");
	window.localStorage.removeItem("cleansed_speedup");
	window.location.reload();
}
