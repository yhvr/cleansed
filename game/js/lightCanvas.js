/** @type {HTMLCanvasElement} */
const lCanvas = document.getElementById("lcanv");
const lctx = lCanvas.getContext("2d");

function updateLight() {
	lctx.fillStyle = "#000000";
	lctx.fillRect(0, 0, 10000, 10000);
	
	lctx.globalCompositeOperation = "destination-out";
	let gradient = lctx.createRadialGradient(
		lCanvas.width / 2 - 255,
		lCanvas.height / 2 - 255,
		500 * Math.PI,
		lCanvas.width / 2 - 5,
		lCanvas.height / 2 - 5,
		10
	);
	gradient.addColorStop(0.9 - Math.log10(game.shards / 100 + 2) / 25, "#00000000");
	gradient.addColorStop(1, "#000000ff");
	lctx.fillStyle = gradient;
	lctx.beginPath();
	lctx.arc(lCanvas.width / 2, lCanvas.height / 2, 500, 0, Math.PI * 2, true);
	lctx.fill();
		
	lctx.globalCompositeOperation = "source-over";
	// lctx.strokeStyle = "#ffffff";
	// lctx.beginPath();
	// lctx.moveTo(lCanvas.width / 2, 0);
	// lctx.lineTo(lCanvas.width / 2, lCanvas.height);
	// lctx.moveTo(0, lCanvas.height / 2);
	// lctx.lineTo(lCanvas.width, lCanvas.height / 2);
	// lctx.stroke();
}
