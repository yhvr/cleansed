const doors = {
	home: {
		"7,2": ["street", 7, 6],
	},
	street: {
		"7,7": ["home", 6, 2],
	},
};

function door(coords) {
	const to = doors[game.currentMap][coords.join()];
	game.currentMap = to[0];
	game.coords = [to[1], to[2]];
}
