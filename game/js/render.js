/** @type {HTMLCanvasElement} */
const canvas = document.getElementById("canv");
const ctx = canvas.getContext("2d");
const canvDiv = document.getElementById("canvdiv")

ctx.font = "25px 'JetBrains Mono', sans-serif";

let lastTick = 0;

const fillColors = {
	"@": "#8888FF",
	".": "#44FF44",
	"⚙": "#444444",
	"★": "#FFFF00",
	1: "#BB88BB",
	2: "#AA77AA",
	3: "#996699",
	4: "#885588",
	5: "#774477",
	6: "#663366",
	7: "#551555",
	8: "#441044",
	9: "#330533",
	0: "#220022",
	"#": "#444444",
	_: "#11CCFF",
	"⌂": "#DD9944",
};

/**
 * e.g
 * mapname: {
 *  "&": "#FFFFFF"
 * }
 */
const fillOverrides = {
	home: {
		"#": "#E08020",
		".": "#78480C",
	},
};

const wholeCanvas = [0, 0, 10000, 10000];

function getEndShake() {
	return isEnd ? Math.random() - 0.5 : 0;
}

let redraw = true;
function nextFrame(tick) {
	let dt = (tick - lastTick) / 1000;
	lastTick = tick;

	checkShardCollection();

	const width = game.maps[game.currentMap][0].length,
		height = game.maps[game.currentMap].length;
	update([width, height]);
	updateAuto([width, height]);

	if (redraw) {
		updateLight();

		// clear rect for rendering
		ctx.fillStyle = "#000000";
		ctx.fillRect(...wholeCanvas);

		const [x, y] = game.coords;
		const map = game.maps[game.currentMap];
		const [size_x, size_y] = canvasSize;
		for (
			let i = x - Math.round(size_x / 2);
			i <= x + Math.round(size_x / 2);
			i++
		) {
			for (
				let j = y - Math.floor(size_y / 2);
				j <= y + Math.round(size_y / 2);
				j++
			) {
				let char = map[j]?.[i];
				if (game.coords[0] === i && game.coords[1] === j) char = "@";
				if (char === undefined) char = " ";

				ctx.fillStyle =
					(fillOverrides[game.currentMap]?.[char] ??
						fillColors[char] ??
						"#FFFFFF") // + calculateAlpha([i, j]);
				if (sweepRow === j) ctx.fillStyle = "#FFFFFF";

				ctx.fillText(
					char,
					(i - (x + .5) + Math.floor(size_x / 2)) * 20 + 3 + getEndShake(),
					(j - (y - 1) + Math.floor(size_y / 2) + 1) * 25 + 2 + getEndShake()
				);
			}
		}
		if (performance.now() > 1000) redraw = false;
	}

	window.requestAnimationFrame(nextFrame);
}

let canvasSize = [13, 13];

function updateCanvasRoom() {
	const { offsetLeft, offsetTop } = app.$el.querySelector("p");
	const width = window.innerWidth - offsetLeft * 2;
	const height = window.innerHeight - (offsetTop * 2 + (isEnd ? 18 : 118));

	canvas.width = width
	canvas.height = height;
	lCanvas.width = width;
	lCanvas.height = height;

	ctx.font = "25px 'JetBrains Mono', sans-serif";
	
	let txtWidth = Math.floor(width / 20);
	let txtHeight = Math.floor((height + 1) / 25 - 3);
	
	canvasSize = [txtWidth, txtHeight];
	redraw = true;
}

updateCanvasRoom();
window.addEventListener("resize", () => updateCanvasRoom());

window.requestAnimationFrame(nextFrame);
