# CLEANSED (2)

CLEANSED was an incremental game made for IGJ2021, a 1-week game jam, where it got 2nd place. For some reason, people liked it, and wanted more, so I'm making more.

[Play the original](https://yhvr.itch.io/cleansed/)